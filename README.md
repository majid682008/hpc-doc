# Documentation for high performance computing (HPC) clusters at UT.
https://majid682008.gitlab.io/hpc-doc/

## Clone

Frist we should clone the repository:
```bash
  git clone https://gitlab.com/majid682008/hpc-doc.git
  cd hpc-doc
```
In `Iran` you must use a proxy to access `gitlab.com` web-site. I recommend `fod`. You could simply use it following this [link](https://github.com/freedomofdevelopers/fod)

## Virtual Environments
To development mode; Just use steps below:
```bash
  python3 -m venv hpc-venv
  source hpc-venv/bin/activate
  pip install -r requirements.txt
  make html
```
